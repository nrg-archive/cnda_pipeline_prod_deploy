#!/usr/bin/env python

import scipy.io
import requests
import warnings
import glob
import sys
import os
import re
from lxml.builder import ElementMaker
from lxml import etree

# Input args
# I'm playing fast and loose with these, but since this will be run from a pipeline I hope any param errors would be caught before now
[project,
 sessionId,
 assessorId,
 assessorLabel,
 datestamp,
 expt_xml_path,
 assessor_xml_path,
 has_rs,
 has_stim,
 rs_samplingrate,
 rs_lowpass,
 rs_highpass,
 stim_samplingrate,
 stim_lowpass,
 stim_highpass,
 stim_blocksize,
 stim_baseline,
 stim_duration] = sys.argv[1:]

# Define namespaces and namespace helper functions
nsdict = {'xnat': 'http://nrg.wustl.edu/xnat',
          'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          'opti': 'http://nrg.wustl.edu/opti'}
def ns(namespace,tag):
    return "{%s}%s"%(nsdict[namespace], tag)
def schemaLoc(namespace):
    return "{0} https://nrg.wustl.edu/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace)


if len(datestamp) >= 8:
    date = '{}-{}-{}'.format(datestamp[0:4], datestamp[4:6], datestamp[6:8])
else:
    date = datestamp

assessorElementsDict = {}
if has_rs == 'true':
    assessorElementsDict.update({
        "rs_samplingrate": rs_samplingrate,
        "rs_lowpass": rs_lowpass,
        "rs_highpass": rs_highpass
    })
if has_stim == 'true':
    assessorElementsDict.update({
        "stim_samplingrate": stim_samplingrate,
        "stim_lowpass": stim_lowpass,
        "stim_highpass": stim_highpass,
        "stim_blocksize": stim_blocksize,
        "stim_baseline": stim_baseline,
        "stim_duration": stim_duration,
    })

assessorTitleAttributesDict = {'ID':assessorId, 'project':project, 'label':assessorLabel,
            ns('xsi','schemaLocation'):' '.join(schemaLoc(namespace) for namespace in ('xnat','opti'))}

sessionElementsDict = {}
scanElementsDict = {}
assessorScanElementsDict = {}

sessionPropertyNamesDict = {
    'numled': 'numled',
    'contrasts_hb': 'hb'
}

scanPropertyNamesDict = {
    'pixels_x': 'nVx',
    'pixels_y': 'nVy',
    'frames_led': 'T1'
}
assessorPropertyNamesDict = {
    'frames_hb': 'T2',
}

# This function is useful to pull values from datahb and upload them
def pullFromDatahb(propertyNamesDict):
    return {our_name: mat['info'][ois_name][0,0][0,0] for our_name, ois_name in propertyNamesDict.iteritems()}

print("Looking for datahb files in "+os.getcwd())
datahbs = glob.glob('*datahb.mat')
if len(datahbs) == 0:
    print "I didn't find any"
    sys.exit("ERROR: No datahb files")

scanIdRe = re.compile(r'.*(fc|stim)(.*)-datahb.mat')

#####
# Read datahb files, pull out info we care about
for datahb in datahbs:
    print "Parsing "+datahb
    mat = scipy.io.loadmat(datahb)

    scanIdMatch = scanIdRe.match(datahb)
    if not scanIdMatch:
        print "Could not find scan ID for datahb file "+datahb
        continue
    scanId = scanIdMatch.group(2)
    print "Setting properties for scan "+scanId

    sessionElementsDict[scanId] = pullFromDatahb(sessionPropertyNamesDict)

    # Get scan info from datahb
    print "Parsing scan-level properties"
    scanElementsDict[scanId] = pullFromDatahb(scanPropertyNamesDict)

    # Get scan-level assessor info from datahb
    print "Parsing scan-level assessor properties"
    assessorScanElementsDict[scanId] = pullFromDatahb(assessorPropertyNamesDict)

# check for numleds, contrasts_hb; if they are not found, write them in
# First, a sanity check. We should have lots of identical values of numled, contrasts_hb
print "Sanity-checking datahbs"
propDicts = sessionElementsDict.values()
if not all([propDict == propDicts[0] for propDict in propDicts]):
    mess = 'ERROR: values for numled, contrasts_hb should be identical for all datahb files.'
    for scanid, propDict in sessionElementsDict.iteritems():
        mess += '\nscanId: {}'
        for name, val in propDict.iteritems():
            mess += '\n\tname: {}\tvalue: {}'.format(name, val)
    sys.exit(mess)
sessionProps = propDicts[0]

#####
# Construct and write assessor XML
E = ElementMaker(namespace=nsdict['opti'], nsmap=nsdict)

assessorXML = E('OISProcessed', assessorTitleAttributesDict,
    E(ns('xnat', 'date'), date),
    E(ns('xnat', 'imageSession_ID'), sessionId),
    E('frames_hbs',
        *[E('frames_hb', {'scanid': scanid}, str(framesHbDict['frames_hb']))
          for scanid, framesHbDict in assessorScanElementsDict.iteritems()]
    ),
    E('inputParams',
        *[E(elName, elValue) for elName, elValue in assessorElementsDict.iteritems()]
    )
)
print "Writing assessor"
with open(assessor_xml_path, 'w') as f:
    f.write(etree.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True))


#####
# Read in experiment XML so we can add necessary elements
print "Reading session XML"
exptXMLIn = etree.parse(expt_xml_path)
experimentXML = exptXMLIn.getroot()


print "Adding session-level properties"
for propName, propVal in sessionProps.iteritems():
    prop = experimentXML.xpath('opti:{}'.format(propName), namespaces=experimentXML.nsmap)
    if not prop:
        experimentXML.append(E(propName, str(propVal)))

# Write scan info
print "Adding scan-level properties"
for scanid, scanElementValuesDict in scanElementsDict.iteritems():
    scanXMLList = experimentXML.xpath('xnat:scans/xnat:scan[@ID="{}"]'.format(scanid), namespaces=experimentXML.nsmap)
    if len(scanXMLList)==0:
        warnings.warn("WARNING: Could not find scan {} in session XML. Cannot add scan properties.".format(scanid))
        continue
    else:
        scanXML = scanXMLList[0]
        for elName, elValue in scanElementValuesDict.iteritems():
            scanXML.append(E(elName, str(elValue)))

print "Writing modified session XML"
with open(expt_xml_path, 'w') as f:
    f.write(etree.tostring(experimentXML, pretty_print=True, encoding='UTF-8', xml_declaration=True))
