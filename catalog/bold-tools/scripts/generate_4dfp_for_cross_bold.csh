#!/bin/csh -x

set idstr = '$Id$'
echo $idstr
set program = $0; set program = $program:t

if (${#argv} < 1) then
	echo "usage:	"$program" param_sfile"
	exit 1
endif
set prmfile = $1
echo "prmfile="$prmfile

if (! -e $prmfile) then
	echo $program": "$prmfile not found
	exit -1
endif
source $prmfile

set endian = 0

if (${#argv} > 1) then
	set endian = $2
endif

if (! ${?usescr}) set usescr = 0


@ runs = ${#irun}
if ($runs != ${#fstd}) then
	echo "irun fstd mismatch - edit "$prmfile
	exit -1
endif

############################
#
#  Generate EPI 4dfp files
#
############################

@ k = 1

while ($k <= $runs)
	if ($usescr) then		# test to see if user requested use of scratch disk
		if (-e bold$irun[$k]) /bin/rm bold$irun[$k]	# remove existing link
		if (! -d $scrdir/bold$irun[$k]) mkdir $scrdir/bold$irun[$k]
		ln -s $scrdir/bold$irun[$k] bold$irun[$k]
	else
		if (! -d bold$irun[$k]) mkdir bold$irun[$k]
	endif

	pushd bold$irun[$k]

	if ($sorted) then
	     set filepath = $inpath/study$fstd[$k]
	else
	     set filepath = $inpath/$dcmroot.$fstd[$k].
	endif

	/data/CNDA/pipeline/catalog/bold-tools/scripts/input_to_4dfp  study$fstd[$k] $filepath DICOM 1 $sorted
	if ($status) exit $status
	if ($endian) endian_4dfp study$fstd[$k] -@b
	if ($status) exit $status
	popd	# out of bold$irun[$k]
	@ k++
end

############################
#
# Generate MPRAGE 4dfp files
#
############################

if (! -d atlas) mkdir atlas

pushd atlas

@ nmpr = ${#mprs}
if ($nmpr < 1) exit 0
@ k = 1
while ($k <= $nmpr)
	if ($sorted) then
	     set filepath = $inpath/study$mprs[$k]
	else
	     set filepath = $inpath/$dcmroot.$mprs[$k].
	endif

	/data/CNDA/pipeline/catalog/bold-tools/scripts/input_to_4dfp  $patid"_mpr"$k $filepath DICOM 1 $sorted
	if ($status) exit $status
	if ($endian) endian_4dfp $patid"_mpr"$k -@b
	if ($status) exit $status
	@ k++
end

############################
#
# Generate T2 4dfp files
#
############################
if (! ${?tse})	set tse = ()
if (! ${?t1w})	set t1w = ()
if (! ${?pdt2})	set pdt2 = ()


@ ntse = ${#tse}
if ($ntse) then
	@ k = 1
	while ($k <= $ntse)
		set filenam = $patid"_t2w"
		if ($ntse > 1) set filenam = $filenam$k
		if ($sorted) then
		     set filepath = $inpath/study$tse[$k]
		else
		     set filepath = $inpath/$dcmroot.$tse[$k].
		endif

		/data/CNDA/pipeline/catalog/bold-tools/scripts/input_to_4dfp  $filenam $filepath  DICOM 1 $sorted
		if ($status) exit $status
		if ($endian) endian_4dfp $filenam -@b
		if ($status) exit $status
		@ k++
	end
else if (${#t1w}) then
		if ($sorted) then
		     set filepath = $inpath/study$t1w
		else
		     set filepath = $inpath/$dcmroot.$t1w.
		endif

		/data/CNDA/pipeline/catalog/bold-tools/scripts/input_to_4dfp $patid"_t1w" $filepath DICOM 1 $sorted
		if ($status) exit $status
		if ($endian) endian_4dfp $patid"_t1w" -@b
		if ($status) exit $status
else if (${#pdt2}) then
		if ($sorted) then
		     set filepath = $inpath/study$pdt2
		else
		     set filepath = $inpath/$dcmroot.$pdt2.
		endif

		/data/CNDA/pipeline/catalog/bold-tools/scripts/input_to_4dfp $patid"_pdt2" $filepath DICOM 1 $sorted
		if ($status) exit $status
		if ($endian) endian_4dfp $patid"_pdt2" -@b
		if ($status) exit $status
endif

exit 0
