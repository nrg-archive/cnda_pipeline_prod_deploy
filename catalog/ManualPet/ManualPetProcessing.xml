<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline
..\..\schema\pipeline.xsd" xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
    <name>ManualPetProcessing</name>
    <!--Should be  Name of the pipeline XML file -->
    <location>ManualPet</location>
    <!-- Filesystem path to the pipeline XML -->
    <description>Processing pipeline for PET. This pipeline doesnt depend on the number of frames. </description>
    <documentation>
        <authors>
            <author>
                <lastname>Jon</lastname>
                <firstname>Christensen</firstname>
            </author>
            <author>
                <lastname>Yi</lastname>
                <firstname>Su</firstname>
            </author>
            <author>
                <lastname>Mohana</lastname>
                <firstname>Ramaratnam</firstname>
            </author>
        </authors>
        <version>1</version>
        <input-parameters>
            <parameter>
                <name>T1</name>
                <values>
                    <csv>MPRAGE,MPRAGE_GRAPPA2,MPRAGE GRAPPA2,MPRAGE GRAPPA2 repeat</csv>
                </values>
                <description>Scantype of the T1 scan</description>
            </parameter>
            <parameter>
                <name>pet_scan_type</name>
                <values>
                    <csv>DIAN PIB 336mtx (AC)</csv>
                </values>
                <description>Scantype of the PET scan</description>
            </parameter>
            <parameter>
                <name>mst</name>
                <values>
                    <csv>40</csv>
                </values>
                <description>Start time for model</description>
            </parameter>
            <parameter>
                <name>mdt</name>
                <values>
                    <csv>30</csv>
                </values>
                <description>Duration of model</description>
            </parameter>
            <parameter>
                <name>hl</name>
                <values>
                    <csv>1223</csv>
                </values>
                <description>Default half-life must be 1223 or 6586.</description>
            </parameter>
        </input-parameters>
    </documentation>
    <xnatInfo appliesTo="xnat:petSessionData"></xnatInfo>
    <outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/PROCESSED/LOGS/',/Pipeline/parameters/parameter[name='pet_sessionId']/values/unique/text())^</outputFileNamePrefix>
    <parameters>
        <!-- Fixed Section -->
        <parameter>
            <name>workdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='pet_sessionId']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>processeddir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/PROCESSED')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mpragedir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='processeddir']/values/unique/text(),'/MPRAGE')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>rawdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW/',/Pipeline/parameters/parameter[name='scan_id']/values/unique/text())^</unique>
            </values>
        </parameter>
        <!-- <parameter>
            <name>JSESSION</name>
            <values>
                <unique>^fileUtils:getJSESSION('DUMMY')^</unique>
            </values>
            <description>JSESSION</description>
        </parameter> -->
    </parameters>
    <steps>
        <step id="0" description="Prepare Folder Structure" >
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='mpragedir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="0a" description="Copy MPRAGE DICOM DATA"  workdirectory="^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="absolutePath"/>
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='mr_xnatId']/values/unique/text(),'/scans/',/Pipeline/parameters/parameter[name='scan_id']/values/unique/text(),'/resources/DICOM/files?format=zip"')^</value>
                </argument>
                <argument id="outfile">
                    <value>scan.zip</value>
                </argument>
            </resource>
            <resource name="unzip" location="commandlineTools">
                <argument id="options">
                    <value>-oj</value>
                </argument>
                <argument id="source">
                    <value>scan.zip</value>
                </argument>
            </resource>
            <resource name="rm" location="commandlineTools">
                <argument id="file">
                    <value>scan.zip</value>
                </argument>
                <argument id="f"/>
            </resource>
        </step>
        <step id="0b-1" description="Create 4dfp file from MPRAGE" workdirectory="^/Pipeline/parameters/parameter[name='mpragedir']/values/unique/text()^">
            <resource name="dcm_to_4dfp" location="nilResources">
                <argument id="q"/>
                <argument id="b">
                    <value>^concat(/Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),'_',/Pipeline/parameters/parameter[name='scan_id']/values/unique/text(),'_mpr')^</value>
                </argument>
                <argument id="file">
                    <value>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/*')^</value>
                </argument>
            </resource>
        </step>
        <step id="RegisterToAtlas" description="Register T1 to atlas space" workdirectory="^/Pipeline/parameters/parameter[name='mpragedir']/values/unique/text()^">
            <resource name="MRITOTARGET" location="ManualPet/resources" >
                <argument id="input">
                    <value>^concat(/Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),'_',/Pipeline/parameters/parameter[name='scan_id']/values/unique/text(),'_mpr')^</value>
                </argument>
            </resource>
        </step>
        <step id="Upload" description="Copy MPRAGE Registered DATA"  workdirectory="^/Pipeline/parameters/parameter[name='processeddir']/values/unique/text()^">
            <resource name="cp" location="commandlineTools">
                <argument id="source">
                    <value>LOGS/*.err</value>
                </argument>
                <argument id="destination">
                    <value>MPRAGE</value>
                </argument>
            </resource>
            <resource name="cp" location="commandlineTools">
                <argument id="source">
                    <value>LOGS/*.log</value>
                </argument>
                <argument id="destination">
                    <value>MPRAGE</value>
                </argument>
            </resource>
            <resource name="zip" location="commandlineTools">
                <argument id="recursive"/>
                <argument id="archive">
                    <value>MPRAGE</value>
                </argument>
                <argument id="folder">
                    <value>MPRAGE</value>
                </argument>
            </resource>
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='pet_xnatId']/values/unique/text(),'/resources/MPRAGE_222/files?extract=true&amp;content=',/Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),'_',/Pipeline/parameters/parameter[name='scan_id']/values/unique/text(),'_MPRAGE222"')^</value>
                </argument>
                <argument id="infile">
                    <value>MPRAGE.zip</value>
                </argument>
            </resource>
        </step>
        <step id="Notify" description="Notify" >
            <resource name="Notifier" location="notifications" >
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="to">
                    <value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
                </argument>
                <argument id="cc">
                    <value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
                </argument>
                <argument id="from">
                    <value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
                </argument>
                <argument id="subject">
                    <value>^concat('CNDA update: ', /Pipeline/parameters/parameter[name='pet_sessionId']/values/unique/text(),' T1 Atlas registered image available')^</value>
                </argument>
                <argument id="host">
                    <value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
                </argument>
                <argument id="body">
                    <value>^concat('Dear ',/Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),',&lt;br&gt; &lt;p&gt;', /Pipeline/parameters/parameter[name='sessionId']/values/unique/text(),' has been registered to the atlas.&lt;/p&gt;&lt;br&gt; &lt;p&gt;Details for this session are available at &lt;a href="',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/app/action/DisplayItemAction/search_element/xnat:petSessionData/search_field/xnat:petSessiondata.ID/search_value/',/Pipeline/parameters/parameter[name='pet_xnatId']/values/unique/text(),'"&gt;the CNDA website.&lt;/a&gt; &lt;/p&gt;&lt;br&gt;CNDA Team.')^
                    </value>
                </argument>
            </resource>
        </step>
    </steps>
</Pipeline>
