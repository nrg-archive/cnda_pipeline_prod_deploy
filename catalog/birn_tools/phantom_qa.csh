#!/bin/tcsh

source ~/.cshrc

if ( ${#argv} < 5 ) then
  echo "Usage: phantom_qa <Site ID> <Site Name> <REQUIRED full path to QA series XCEDE file> <QCAssessment File> <scan id>"
  exit -1;
endif

if (! -e $3) then
  echo "Error: native series XCEDE file $3 does not exist"
  exit -1
endif

if (! -e $4) then
  echo "Error: QCAssessment File doesnt exist. Cannt append to it"
  exit -1
endif


set siteID="$1"
set siteName="$2"
set bxhOutputFile="$3:t"
set seriesFullPath="$3:h"
set seriesLabel="$seriesFullPath:t"

set qcAssessmentFile=$4
set scanId=$5

set bindir=$BXH_XCEDE_TOOLS_HOME/bin


echo "Working on directory\n$seriesFullPath"
echo "Working on series\n$seriesLabel"
echo "Working on native XCEDE file\n$bxhOutputFile"


#############################################################################
#  set up the output directory to Derived_Data/BIRN_QA
#
if( !(-d $seriesFullPath/scan$scanId) ) then
  exit 1;
endif

set derivedDataQA="$seriesFullPath/scan$scanId"
if( !(-d $derivedDataQA) ) then
  echo "Error,\ncould not generate output directory $derivedDataQA\n"
  exit -1;
endif


#############################################################################
#  output files
#
set outputXMLFileName="$derivedDataQA/stabilityQA.xml"
set outputHTMLFileName="$derivedDataQA/index.html"
set outputLogFileName="$derivedDataQA/stabilityQA.log"
set outputTmpAcqDataFileName="$derivedDataQA/tmpacqdata.txt"


#############################################################################
#  The protocol should be run at 200 measurements; however, sometimes it
#  is run at either lower or higher than this number.  An odd number of
#  measurements must be evaluated to correctly assess the results
#  query the XML file for <dimension type="t"> then look
#  at the size entry to find out how many time points.
#  This procedure below does not in anyway correctly parse the XML file
#  If the layout of the XML file changes, the procedure could fail.
#  Future version of this script should correctly parse the file to get the
#  dimension type t value in a query.
#  Perl version of phantom qa indexes time points starting at 0.
#
#  UPDATE: the script now defaults to removing the first 2 or 3 timepoints
#  so the code that calculates this is now removed
#

#############################################################################
#  run analysis script
#
set date=`date`
echo $bindir/fmriqa_phantomqa.pl "$seriesFullPath/$bxhOutputFile" --overwrite "$derivedDataQA"

$bindir/fmriqa_phantomqa.pl "$seriesFullPath/$bxhOutputFile" --overwrite "$derivedDataQA" >&! $outputLogFileName


#############################################################################
#  retrieve data from OS and/or script output
#
set programName="fmriqa_phantomqa.pl"
set programArgument=""'"'"$seriesFullPath/$bxhOutputFile"'"'""
set version=`$bindir/fmriqa_phantomqa.pl --version`
set timeStamp="$date"
set user="$USER"
set machine=`hostname`
set platform=`grep "model name" /proc/cpuinfo | awk 'BEGIN{FS=":"}{print $2}'`
set platformVersion=`grep "Linux" /proc/version`

set Mean=`grep "#mean=" $outputLogFileName | awk -F= '{print $2}'`
set SNR=`grep "#SNR=" $outputLogFileName | awk -F= '{print $2}'`
set SFNR=`grep "#SFNR=" $outputLogFileName | awk -F= '{print $2}'`
set StdDev=`grep "#std=" $outputLogFileName | awk -F= '{print $2}'`
set PercentFluctuation=`grep "#percentFluc=" $outputLogFileName | awk -F= '{print $2}'`
set Drift=`grep "#drift=" $outputLogFileName | awk -F= '{print $2}'`
rm -f $outputTmpAcqDataFileName
grep "##acquisitiondata:" $outputLogFileName | perl -pe 's/##acquisitiondata:([^ ]+) = (.*)/<item name="$1">$2<\/item>/' > $outputTmpAcqDataFileName

set origdims=`grep "##orig. dimensions: " $outputLogFileName | sed 's/.*dimensions: //'`

set mincmassx=`grep "#mincmassx=" $outputLogFileName | awk -F= '{print $2}'`
set mincmassy=`grep "#mincmassy=" $outputLogFileName | awk -F= '{print $2}'`
set mincmassz=`grep "#mincmassz=" $outputLogFileName | awk -F= '{print $2}'`
set maxcmassx=`grep "#maxcmassx=" $outputLogFileName | awk -F= '{print $2}'`
set maxcmassy=`grep "#maxcmassy=" $outputLogFileName | awk -F= '{print $2}'`
set maxcmassz=`grep "#maxcmassz=" $outputLogFileName | awk -F= '{print $2}'`
set meancmassx=`grep "#meancmassx=" $outputLogFileName | awk -F= '{print $2}'`
set meancmassy=`grep "#meancmassy=" $outputLogFileName | awk -F= '{print $2}'`
set meancmassz=`grep "#meancmassz=" $outputLogFileName | awk -F= '{print $2}'`
set dispcmassx=`grep "#dispcmassx=" $outputLogFileName | awk -F= '{print $2}'`
set dispcmassy=`grep "#dispcmassy=" $outputLogFileName | awk -F= '{print $2}'`
set dispcmassz=`grep "#dispcmassz=" $outputLogFileName | awk -F= '{print $2}'`
set driftcmassx=`grep "#driftcmassx=" $outputLogFileName | awk -F= '{print $2}'`
set driftcmassy=`grep "#driftcmassy=" $outputLogFileName | awk -F= '{print $2}'`
set driftcmassz=`grep "#driftcmassz=" $outputLogFileName | awk -F= '{print $2}'`
set minfwhmx=`grep "#minfwhmx=" $outputLogFileName | awk -F= '{print $2}'`
set minfwhmy=`grep "#minfwhmy=" $outputLogFileName | awk -F= '{print $2}'`
set minfwhmz=`grep "#minfwhmz=" $outputLogFileName | awk -F= '{print $2}'`
set maxfwhmx=`grep "#maxfwhmx=" $outputLogFileName | awk -F= '{print $2}'`
set maxfwhmy=`grep "#maxfwhmy=" $outputLogFileName | awk -F= '{print $2}'`
set maxfwhmz=`grep "#maxfwhmz=" $outputLogFileName | awk -F= '{print $2}'`
set meanfwhmx=`grep "#meanfwhmx=" $outputLogFileName | awk -F= '{print $2}'`
set meanfwhmy=`grep "#meanfwhmy=" $outputLogFileName | awk -F= '{print $2}'`
set meanfwhmz=`grep "#meanfwhmz=" $outputLogFileName | awk -F= '{print $2}'`
set meanghost=`grep "#meanghost=" $outputLogFileName | awk -F= '{print $2}'`
set meanbrightghost=`grep "#meanbrightghost=" $outputLogFileName | awk -F= '{print $2}'`

set rdc=`grep "#rdc=" $outputLogFileName | awk -F= '{print $2}'`


#############################################################################
#  export script output to well-formed derived data xml file
#
echo "<serieslevel xmlns="\""http://www.nbirn.net/schemas/fbirnxml"\"\
  " xmlns:xsi="\""http://www.w3.org/2001/XMLSchema-instance"\"">" \
  >!  $outputXMLFileName

echo "<provenance id="\"${seriesLabel}\"">" \
  >>  $outputXMLFileName

echo "<processStep>" \
  >> $outputXMLFileName

echo "<programName>${programName}</programName>" \
  >> $outputXMLFileName

echo "<programArgument>${programArgument}</programArgument>" \
  >> $outputXMLFileName

echo "<version>${version}</version>" \
  >> $outputXMLFileName

echo "<timeStamp>${timeStamp}</timeStamp>" \
  >> $outputXMLFileName

echo "<user>${user}</user>" \
  >> $outputXMLFileName

echo "<machine>${machine}</machine>" \
  >> $outputXMLFileName

echo "<platform>${platform}</platform>" \
  >> $outputXMLFileName

echo "<platformVersion>${platformVersion}</platformVersion>" \
  >> $outputXMLFileName

echo "</processStep>" \
  >> $outputXMLFileName

echo "</provenance>" \
  >> $outputXMLFileName

echo "<statistic xsi:type="\""simpleStatisticList_t"\"">" \
  >> $outputXMLFileName

echo "<sourceData>${bxhOutputFile}</sourceData>" \
  >> $outputXMLFileName

echo "<process>BIRN QA</process>" \
  >> $outputXMLFileName

echo "<provenanceID>${seriesLabel}</provenanceID>" \
  >> $outputXMLFileName

echo "<description>Simple output from BIRN QA</description>" \
  >> $outputXMLFileName

echo "<item name="\""siteID"\"" type="\""int"\"">${siteID}</item>" \
  >> $outputXMLFileName

echo "<item name="\""siteName"\"" type="\""varchar"\"">${siteName}</item>" \
  >> $outputXMLFileName

echo "<item name="\""origdims"\"" type="\""varchar"\"">${origdims}</item>" \
  >> $outputXMLFileName

echo "<item name="\""Mean"\"" type="\""float"\"">${Mean}</item>" \
  >> $outputXMLFileName

echo "<item name="\""SNR"\"" type="\""float"\"">${SNR}</item>" \
  >> $outputXMLFileName

echo "<item name="\""SFNR"\"" type="\""float"\"">${SFNR}</item>" \
  >> $outputXMLFileName

echo "<item name="\""StdDev"\"" type="\""float"\"">${StdDev}</item>" \
  >> $outputXMLFileName

echo "<item name="\""PercentFluctuation"\"" type="\""float"\"">${PercentFluctuation}</item>" \
  >> $outputXMLFileName

echo "<item name="\""Drift"\"" type="\""float"\"">${Drift}</item>" \
  >> $outputXMLFileName

cat $outputTmpAcqDataFileName >> $outputXMLFileName

echo "<item name="\""rdc"\"" type="\""float"\"">${rdc}</item>" >> $outputXMLFileName

echo "<item name="\""mincmassx"\"" type="\""float"\"">${mincmassx}</item>" >> $outputXMLFileName
echo "<item name="\""mincmassy"\"" type="\""float"\"">${mincmassy}</item>" >> $outputXMLFileName
echo "<item name="\""mincmassz"\"" type="\""float"\"">${mincmassz}</item>" >> $outputXMLFileName
echo "<item name="\""maxcmassx"\"" type="\""float"\"">${maxcmassx}</item>" >> $outputXMLFileName
echo "<item name="\""maxcmassy"\"" type="\""float"\"">${maxcmassy}</item>" >> $outputXMLFileName
echo "<item name="\""maxcmassz"\"" type="\""float"\"">${maxcmassz}</item>" >> $outputXMLFileName
echo "<item name="\""meancmassx"\"" type="\""float"\"">${meancmassx}</item>" >> $outputXMLFileName
echo "<item name="\""meancmassy"\"" type="\""float"\"">${meancmassy}</item>" >> $outputXMLFileName
echo "<item name="\""meancmassz"\"" type="\""float"\"">${meancmassz}</item>" >> $outputXMLFileName
echo "<item name="\""dispcmassx"\"" type="\""float"\"">${dispcmassx}</item>" >> $outputXMLFileName
echo "<item name="\""dispcmassy"\"" type="\""float"\"">${dispcmassy}</item>" >> $outputXMLFileName
echo "<item name="\""dispcmassz"\"" type="\""float"\"">${dispcmassz}</item>" >> $outputXMLFileName
echo "<item name="\""driftcmassx"\"" type="\""float"\"">${driftcmassx}</item>" >> $outputXMLFileName
echo "<item name="\""driftcmassy"\"" type="\""float"\"">${driftcmassy}</item>" >> $outputXMLFileName
echo "<item name="\""driftcmassz"\"" type="\""float"\"">${driftcmassz}</item>" >> $outputXMLFileName
echo "<item name="\""minfwhmx"\"" type="\""float"\"">${minfwhmx}</item>" >> $outputXMLFileName
echo "<item name="\""minfwhmy"\"" type="\""float"\"">${minfwhmy}</item>" >> $outputXMLFileName
echo "<item name="\""minfwhmz"\"" type="\""float"\"">${minfwhmz}</item>" >> $outputXMLFileName
echo "<item name="\""maxfwhmx"\"" type="\""float"\"">${maxfwhmx}</item>" >> $outputXMLFileName
echo "<item name="\""maxfwhmy"\"" type="\""float"\"">${maxfwhmy}</item>" >> $outputXMLFileName
echo "<item name="\""maxfwhmz"\"" type="\""float"\"">${maxfwhmz}</item>" >> $outputXMLFileName
echo "<item name="\""meanfwhmx"\"" type="\""float"\"">${meanfwhmx}</item>" >> $outputXMLFileName
echo "<item name="\""meanfwhmy"\"" type="\""float"\"">${meanfwhmy}</item>" >> $outputXMLFileName
echo "<item name="\""meanfwhmz"\"" type="\""float"\"">${meanfwhmz}</item>" >> $outputXMLFileName
echo "<item name="\""meanghost"\"" type="\""float"\"">${meanghost}</item>" >> $outputXMLFileName
echo "<item name="\""meanbrightghost"\"" type="\""float"\"">${meanbrightghost}</item>" >> $outputXMLFileName


echo "<item name="\""screenshot"\"" type="\""URI"\"">$outputHTMLFileName:t</item>" \
  >> $outputXMLFileName

echo "</statistic>" \
  >> $outputXMLFileName

echo "</serieslevel>" \
  >> $outputXMLFileName

#############################################################################
#  export script output to XNAT QCAssessment derived data xml file
#

echo "<xnat:scan id="\""$scanId"\"">" >> $qcAssessmentFile

echo "<xnat:scanStatistics xsi:type="\""xnat:statisticsData"\"">" >>  $qcAssessmentFile
echo "<xnat:mean>${Mean}</xnat:mean>" >>  $qcAssessmentFile
echo "<xnat:snr>${SNR}</xnat:snr>" >>  $qcAssessmentFile
echo "<xnat:stddev>${StdDev}</xnat:stddev>" >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""SFNR"\"">${SFNR}</xnat:additionalStatistics>" >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""PercentFluctuation"\"">${PercentFluctuation}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""Drift"\"">${Drift}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""rdc"\"">${rdc}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""mincmassx"\"">${mincmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""mincmassy"\"">${mincmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""mincmassz"\"">${mincmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""maxcmassx"\"">${maxcmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxcmassy"\"">${maxcmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxcmassz"\"">${maxcmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""meancmassx"\"">${meancmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meancmassy"\"">${meancmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meancmassz"\"">${meancmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""dispcmassx"\"">${dispcmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""dispcmassy"\"">${dispcmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""dispcmassz"\"">${dispcmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""driftcmassx"\"">${driftcmassx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""driftcmassy"\"">${driftcmassy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""driftcmassz"\"">${driftcmassz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""minfwhmx"\"">${minfwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""minfwhmy"\"">${minfwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""minfwhmz"\"">${minfwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""maxfwhmx"\"">${maxfwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxfwhmy"\"">${maxfwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""maxfwhmz"\"">${maxfwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""meanfwhmx"\"">${meanfwhmx}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meanfwhmy"\"">${meanfwhmy}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meanfwhmz"\"">${meanfwhmz}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

echo "<xnat:additionalStatistics name="\""meanghost"\"">${meanghost}</xnat:additionalStatistics>"   >>  $qcAssessmentFile
echo "<xnat:additionalStatistics name="\""meanbrightghost"\"">${meanbrightghost}</xnat:additionalStatistics>"   >>  $qcAssessmentFile

#echo "<xnat:addField name="\""HTML"\"">$outputHTMLFileName</xnat:addField>" >> $qcAssessmentFile

echo "</xnat:scanStatistics>" >>  $qcAssessmentFile
echo "</xnat:scan>" >> $qcAssessmentFile

rm -f $outputTmpAcqDataFileName
exit 0
