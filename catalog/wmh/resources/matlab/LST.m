function LST(t1,flair)

spm('defaults', 'FMRI');
spm_jobman('initcfg');
clear matlabbatch;

matlabbatch{1}.spm.tools.LST.lesiongrow.data_T1 = {strcat(t1,',1')};
matlabbatch{1}.spm.tools.LST.lesiongrow.data_FLAIR = {strcat(flair,',1')};
matlabbatch{1}.spm.tools.LST.lesiongrow.segopts.initial = [0.3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
matlabbatch{1}.spm.tools.LST.lesiongrow.segopts.belief = 3;
matlabbatch{1}.spm.tools.LST.lesiongrow.segopts.mrf = 1;
matlabbatch{1}.spm.tools.LST.lesiongrow.segopts.maxiter = 50;
matlabbatch{1}.spm.tools.LST.lesiongrow.segopts.threshold = 0;
matlabbatch{1}.spm.tools.LST.lesiongrow.output.lesions.prob = 1;
matlabbatch{1}.spm.tools.LST.lesiongrow.output.lesions.binary = 1;
matlabbatch{1}.spm.tools.LST.lesiongrow.output.lesions.normalized = 1;
matlabbatch{1}.spm.tools.LST.lesiongrow.output.other = 1;

spm_jobman('run', matlabbatch)
