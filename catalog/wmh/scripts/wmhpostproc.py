#!/usr/bin/env python

import scipy.io
import requests
import warnings
import glob
import sys
import os
import re
from lxml.builder import ElementMaker
from lxml import etree

# Input args
# I'm playing fast and loose with these, but since this will be run from a pipeline I hope any param errors would be caught before now
[project,
 sessionId,
 assessorId,
 assessorLabel,
 assessor_xml_path,
 datestamp,
 t1,
 flairSessionId,
 flair,
 measures_txt_file] = sys.argv[1:]

# Define namespaces and namespace helper functions
nsdict = {'xnat': 'http://nrg.wustl.edu/xnat',
          'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          'wmh': 'http://nrg.wustl.edu/wmh'}
def ns(namespace,tag):
    return "{%s}%s"%(nsdict[namespace], tag)
def schemaLoc(namespace):
    return "{0} https://nrg.wustl.edu/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace)


if len(datestamp) >= 8:
    date = '{}-{}-{}'.format(datestamp[0:4], datestamp[4:6], datestamp[6:8])
else:
    date = datestamp

# Read input file
with open(measures_txt_file, 'r') as f:
    values = f.readline().strip()
    voxels = values.split(" ")[0]
    volume = values.split(" ")[1]

assessorElementsDict = {"t1_session": sessionId, "t1_scan": t1, "flair_session": flairSessionId, "flair_scan":flair}

assessorTitleAttributesDict = {'ID':assessorId, 'project':project, 'label':assessorLabel,
            ns('xsi','schemaLocation'):' '.join(schemaLoc(namespace) for namespace in ('xnat','wmh'))}

#####
# Construct and write assessor XML
E = ElementMaker(namespace=nsdict['wmh'], nsmap=nsdict)

assessorXML = E('WMH', assessorTitleAttributesDict,
    E(ns('xnat', 'date'), date),
    E(ns('xnat', 'imageSession_ID'), sessionId),
    E('volume', volume),
    E('voxels', voxels),
    E('inputs',
        *[E(elName, elValue) for elName, elValue in assessorElementsDict.iteritems()]
    )
)
print "Writing assessor"
with open(assessor_xml_path, 'w') as f:
    f.write(etree.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True))

