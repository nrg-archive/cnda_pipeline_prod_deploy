#!/usr/bin/python

# import re
import sys
import argparse

versionNumber="2"
dateString="2014/02/24 10:20:00"
author="flavin"
progName=sys.argv[0].split('/')[-1]
idstring = "$Id: %s,v %s %s %s Exp $"%(progName,versionNumber,dateString,author)

#######################################################
# PARSE INPUT ARGS
parser = argparse.ArgumentParser(description="Generate param file for PUP processing from bash-formatted pipeline params")
parser.add_argument("-v", "--version",
                    help="Print version number and exit",
                    action="version",
                    version=versionNumber)
parser.add_argument("--idstring",
                    help="Print id string and exit",
                    action="version",
                    version=idstring)
parser.add_argument("paramFilePathIn",
                    help="Path to input params file")
parser.add_argument("paramFilePathOut",
                    help="Path to output params file")
parser.add_argument("halflifeCSVPath",
                    help="Path to file mapping tracer -> half life")
parser.add_argument("filterCSVPath",
                    help="Path to file mapping scanner -> filterxy/filterz")
args=parser.parse_args()
#######################################################


print idstring

# This will hold all the params to write out
paramString = ''

# Parse the bash params file. Pull out the params we need for lookup.
tracer = ''
scanner = ''
doFiltering = None

print ''
print 'Reading params from file %s'%args.paramFilePathIn
with open(args.paramFilePathIn,'r') as paramFile:

    # We will not write out comment lines or lines without '='
    strippedLines = (line for line in paramFile if line[0] != '#' and '=' in line)

    for line in strippedLines:
        paramName,paramVal=line.strip('\n').split('=')

        if paramVal=='':
            continue # Do not write lines with a blank paramVal

        if paramName=='tracer':
            tracer = paramVal
        elif paramName=='scanner':
            scanner = paramVal
            continue # Do not write out 'scanner'
        elif paramName=='filter':
            doFiltering = paramVal=='1'

        paramString += line

# Validate that params were found
for param in ('tracer','scanner'):
    if param=='':
        sys.exit('%s ERROR: Required parameter "%s" is not defined in param file %s'%(progName,param,args.paramFilePathIn))
if doFiltering==None:
    sys.exit('%s ERROR: Required parameter "filter" is not defined in param file %s'%(progName,args.paramFilePathIn))

# This will store the params to write out
paramDict = {}

# Look up half-life
half_life=''
print ''
print 'Looking up half life for tracer %s in file %s'%(tracer,args.halflifeCSVPath)
try:
    with open(args.halflifeCSVPath,'r') as halflifeCSV:
        strippedLines = (line.strip('\n') for line in halflifeCSV if line[0] != '#' and ',' in line)
        for line in strippedLines:
            tr,hl=line.split(',')
            if tracer.lower() == tr.lower():
                print 'Found tracer %s'%tr
                print 'Found half life %s'%hl
                if half_life=='':
                    half_life=hl
                elif half_life==hl:
                    print 'Identical to previously found value'
                else:
                    sys.exit('%s ERROR: multiple matches found for tracer %s in file %s'%(progName,tracer,args.halflifeCSVPath))
except IOError:
    sys.exit('%s ERROR: Could not open file %s'%(progName,args.halflifeCSVPath))
if half_life == '':
    sys.exit('%s ERROR: no half life found for tracer %s in file %s'%(progName,tracer,args.halflifeCSVPath))

paramDict["half_life"]=half_life

# Look up filtering params filterxy,filterz if required
print ''
if doFiltering:
    print 'Param "filter"=1. Looking up filterxy,filterz.'
    filterxy,filterz='',''
    print 'Looking for scanner %s in file %s'%(scanner,args.filterCSVPath)
    try:
        with open(args.filterCSVPath,'r') as filterCSV:
            strippedLines = (line.strip('\n') for line in filterCSV if line[0] != '#' and ',' in line)
            for line in strippedLines:
                sc,fxy,fz=line.split(',')
                if scanner.lower() == sc.lower():
                    print 'Found scanner %s'%sc
                    print 'Found filterxy %s, filterz %s'%(fxy,fz)
                    if filterxy=='' and filterz=='':
                        filterxy=fxy
                        filterz=fz
                    elif filterxy==fxy and filterz==fz:
                        print 'Identical to previously found values'
                    else:
                        sys.exit('%s ERROR: multiple matches found for scanner %s in file %s'%(progName,scanner,args.filterCSVPath))
    except IOError:
        sys.exit('%s ERROR: Could not open file %s'%(progName,args.filterCSVPath))
    if filterxy == '' or filterz == '':
        sys.exit('%s ERROR: no match found for scanner %s in file %s'%(progName,scanner,args.filterCSVPath))

    paramDict["filterxy"]=filterxy
    paramDict["filterz"]=filterz
else:
    print 'Param "filter"!=1. Not looking up filterxy, filterz.'


# Write params into param file
paramString += '\n'.join( ['='.join(kvtuple) for kvtuple in paramDict.iteritems()] )+'\n'
print ''
print 'WRITING PARAM FILE %s'%args.paramFilePathOut
with open(args.paramFilePathOut,'w') as paramFile:
    paramFile.write(paramString)

