import argparse
import collections
import json
import requests
import os
import sys
import subprocess
import time
import zipfile
import tempfile
import dicom as dicomLib
from shutil import copy as fileCopy
from nipype.interfaces.dcm2nii import Dcm2nii
from collections import OrderedDict
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()


def cleanServer(server):
    server.strip()
    if server[-1] == '/':
        server = server[:-1]
    if server.find('http') == -1:
        server = 'https://' + server
    return server


def isTrue(arg):
    return arg is not None and (arg == 'Y' or arg == '1' or arg == 'True')


def download(name, pathDict):
    if os.access(pathDict['absolutePath'], os.R_OK):
        try:
            os.symlink(pathDict['absolutePath'], name)
        except:
            fileCopy(pathDict['absolutePath'], name)
            print 'Copied %s.' % pathDict['absolutePath']
    else:
        with open(name, 'wb') as f:
            r = get(pathDict['URI'], stream=True)

            for block in r.iter_content(1024):
                if not block:
                    break

                f.write(block)
        print 'Downloaded file %s.' % name

def zipdir(dirPath=None, zipFilePath=None, includeDirInZip=True):
    if not zipFilePath:
        zipFilePath = dirPath + ".zip"
    if not os.path.isdir(dirPath):
        raise OSError("dirPath argument must point to a directory. "
            "'%s' does not." % dirPath)
    parentDir, dirToZip = os.path.split(dirPath)
    def trimPath(path):
        archivePath = path.replace(parentDir, "", 1)
        if parentDir:
            archivePath = archivePath.replace(os.path.sep, "", 1)
        if not includeDirInZip:
            archivePath = archivePath.replace(dirToZip + os.path.sep, "", 1)
        return os.path.normcase(archivePath)
    outFile = zipfile.ZipFile(zipFilePath, "w",
        compression=zipfile.ZIP_DEFLATED)
    for (archiveDirPath, dirNames, fileNames) in os.walk(dirPath):
        for fileName in fileNames:
            filePath = os.path.join(archiveDirPath, fileName)
            outFile.write(filePath, trimPath(filePath))
        # Make sure we get empty directories as well
        if not fileNames and not dirNames:
            zipInfo = zipfile.ZipInfo(trimPath(archiveDirPath) + "/")
            # some web sites suggest doing
            # zipInfo.external_attr = 16
            # or
            # zipInfo.external_attr = 48
            # Here to allow for inserting an empty directory.  Still TBD/TODO.
            outFile.writestr(zipInfo, "")
    outFile.close()

BIDSVERSION = "1.0.1"

parser = argparse.ArgumentParser(description="Run dcm2niix on every file in a session")
parser.add_argument("--host", default="https://cnda.wustl.edu", help="CNDA host", required=True)
parser.add_argument("--user", help="CNDA username", required=True)
parser.add_argument("--password", help="Password", required=True)
parser.add_argument("--session", help="Session ID", required=True)
parser.add_argument("--subject", help="Subject Label", required=False)
parser.add_argument("--project", help="Project", required=False)
parser.add_argument("--dicomdir", help="Root output directory for DICOM files", required=True)
parser.add_argument("--niftidir", help="Root output directory for NIFTI files", required=True)
parser.add_argument("--overwrite", help="Overwrite NIFTI files if they exist")
parser.add_argument("--upload-by-ref", help="Upload \"by reference\". Only use if your host can read your file system.")
parser.add_argument("--bids-series-map", help="Local copy of the json file mapping series description to BIDS name. Will be merged with site-level and project-level bids/bidsmap configs (if they exist). In case of conflict, site config overrides local file, and project config overrides both.")
parser.add_argument("--workflowId", help="Pipeline workflow ID")
parser.add_argument('--version', action='version', version='%(prog)s 1')

args = parser.parse_args()
host = cleanServer(args.host)
session = args.session
subject = args.subject
project = args.project
overwrite = isTrue(args.overwrite)
dicomdir = args.dicomdir
niftidir = args.niftidir
workflowId = args.workflowId
uploadByRef = isTrue(args.upload_by_ref)
bidsMapPath = args.bids_series_map

imgdir = niftidir + "/IMG"
bidsdir = niftidir + "/BIDS"

builddir = os.getcwd()

# Set up working directory
if not os.access(dicomdir, os.R_OK):
    print 'Making DICOM directory %s' % dicomdir
    os.mkdir(dicomdir)
if not os.access(niftidir, os.R_OK):
    print 'Making NIFTI directory %s' % niftidir
    os.mkdir(niftidir)
if not os.access(imgdir, os.R_OK):
    print 'Making NIFTI image directory %s' % imgdir
    os.mkdir(imgdir)
if not os.access(bidsdir, os.R_OK):
    print 'Making NIFTI BIDS directory %s' % bidsdir
    os.mkdir(bidsdir)

# Set up session
sess = requests.Session()
sess.verify = False
sess.auth = (args.user, args.password)


def get(url, **kwargs):
    try:
        r = sess.get(url, **kwargs)
        r.raise_for_status()
    except (requests.ConnectionError, requests.exceptions.RequestException) as e:
        print "Request Failed"
        print "    " + str(e)
        sys.exit(1)
    return r

if project is None or subject is None:
    # Get project ID and subject ID from session JSON
    print "Get project and subject ID for session ID %s." % session
    r = get(host + "/data/experiments/%s" % session, params={"format": "json", "handler": "values", "columns": "project,subject_ID"})
    sessionValuesJson = r.json()["ResultSet"]["Result"][0]
    project = sessionValuesJson["project"] if project is None else project
    subjectID = sessionValuesJson["subject_ID"]
    print "Project: " + project
    print "Subject ID: " + subjectID

    if subject is None:
        print
        print "Get subject label for subject ID %s." % subjectID
        r = get(host + "/data/subjects/%s" % subjectID, params={"format": "json", "handler": "values", "columns": "label"})
        subject = r.json()["ResultSet"]["Result"][0]["label"]
        print "Subject label: " + subject

# Get list of scan ids
print
print "Get scan list for session ID %s." % session
r = get(host + "/data/experiments/%s/scans" % session, params={"format": "json"})
scanRequestResultList = r.json()["ResultSet"]["Result"]
scanIDList = [scan['ID'] for scan in scanRequestResultList]
seriesDescList = [scan['series_description'] for scan in scanRequestResultList]  # { id: sd for (scan['ID'], scan['series_description']) in scanRequestResultList }
print 'Found scans %s.' % ', '.join(scanIDList)
print 'Series descriptions %s' % ', '.join(seriesDescList)

# Fall back on scan type if series description field is empty
if set(seriesDescList) == set(['']):
    seriesDescList = [scan['type'] for scan in scanRequestResultList]
    print 'Fell back to scan types %s' % ', '.join(seriesDescList)

# Get site- and project-level configs
print
print "Reading BIDS map from file " + bidsMapPath
bidsmaplist = []
if bidsMapPath and os.access(bidsMapPath, os.R_OK):
    with open(bidsMapPath, 'r') as bidsMapFile:
        bidsmaplist += json.load(bidsMapFile)
    print "Done"
else:
    print "Could not read file"

print "Get site-wide BIDS map"
# We don't use the convenience get() method because that throws exceptions when the object is not found.
r = sess.get(host + "/data/config/bids/bidsmap", params={"contents": True})
if r.ok:
    bidsmaptoadd = r.json()
    for mapentry in bidsmaptoadd:
        if mapentry not in bidsmap:
            bidsmap.append(mapentry)
else:
    print "Could not read site-wide BIDS map"

print "Get project BIDS map if one exists"
r = sess.get(host + "/data/projects/%s/config/bids/bidsmap" % project, params={"contents": True})
if r.ok:
    bidsmaptoadd = r.json()
    for mapentry in bidsmaptoadd:
        if mapentry not in bidsmap:
            bidsmap.append(mapentry)
else:
    print "Could not read project BIDS map"

# print "BIDS map: " + json.dumps(bidsmaplist)

# Collapse human-readable JSON to dict for processing
bidsnamemap = {x['series_description'].lower(): x['bidsname'] for x in bidsmaplist}

# Map all series descriptions to BIDS names (case insensitive)
resolved = [bidsnamemap[x.lower()] for x in seriesDescList if x.lower() in bidsnamemap]

# Count occurrences
bidscount = collections.Counter(resolved)

# Remove multiples
multiples = {seriesdesc: count for seriesdesc, count in bidscount.viewitems() if count > 1}

# Cheat and reverse scanid and seriesdesc lists so numbering is in the right order
for scanid, seriesdesc in zip(reversed(scanIDList), reversed(seriesDescList)):
    print
    print 'Beginning process for scan %s.' % scanid
    os.chdir(builddir)

    print 'Assigning BIDS name for scan %s.' % scanid

    # BIDS subject name
    base = "sub-" + subject + "_"

    if seriesdesc.lower() not in bidsnamemap:
        print "Series " + seriesdesc + " not found in BIDSMAP"
        # bidsname = "Z"
        continue  # Exclude series from processing
    else:
        print "Series " + seriesdesc + " matched " + bidsnamemap[seriesdesc.lower()]
        match = bidsnamemap[seriesdesc.lower()]

    # split before last _
    splitname = match.split("_")

    # Check for multiples
    if match in multiples:
        # insert run-0x
        run = 'run-%02d' % multiples[match]
        splitname.insert(len(splitname) - 1, run)

        # decrement count
        multiples[match] -= 1

        # rejoin as string
        bidsname = "_".join(splitname)
    else:
        bidsname = match

    # Get scan resources
    print "Get scan resources for scan %s." % scanid
    r = get(host + "/data/experiments/%s/scans/%s/resources" % (session, scanid), params={"format": "json"})
    scanResources = r.json()["ResultSet"]["Result"]
    print 'Found resources %s.' % ', '.join(res["label"] for res in scanResources)

    ##########
    # Do initial checks to determine if scan should be skipped
    hasNifti = any([res["label"] == "NIFTI" for res in scanResources])  # Store this for later
    if hasNifti and not overwrite:
        print "Scan %s has a preexisting NIFTI resource, and I am running with overwrite=False. Skipping." % scanid
        continue

    dicomResourceList = [res for res in scanResources if res["label"] == "DICOM"]
    if len(dicomResourceList) == 0:
        print "Scan %s has no DICOM resource. Skipping." % scanid
        # scanInfo['hasDicom'] = False
        continue
    elif len(dicomResourceList) > 1:
        print "Scan %s has more than one DICOM resource. Skipping." % scanid
        # scanInfo['hasDicom'] = False
        continue

    dicomResource = dicomResourceList[0]
    if dicomResource["file_count"]:
        if int(dicomResource["file_count"]) == 0:
            print "DICOM resource for scan %s has no files. Skipping." % scanid
            # scanInfo['hasDicom'] = True
            continue
    else:
        print "DICOM resource for scan %s has a blank \"file_count\", so I cannot check it to see if there are no files. I am not skipping the scan, but this may lead to errors later if there are no files." % scanid

    ##########
    # Prepare DICOM directory structure
    print
    scanDicomDir = os.path.join(dicomdir, scanid)
    if not os.path.isdir(scanDicomDir):
        print 'Making scan DICOM directory %s.' % scanDicomDir
        os.mkdir(scanDicomDir)
    # Remove any existing files in the builddir.
    # This is unlikely to happen in any environment other than testing.
    for f in os.listdir(scanDicomDir):
        os.remove(os.path.join(scanDicomDir, f))

    ##########
    # Get list of DICOMs
    print 'Get list of DICOM files for scan %s.' % scanid
    r = get(host + "/data/experiments/%s/scans/%s/resources/DICOM/files" % (session, scanid), params={"format": "json"})
    # I don't like the results being in a list, so I will build a dict keyed off file name
    dicomFileDict = {dicom['Name']: {'URI': host + dicom['URI']} for dicom in r.json()["ResultSet"]["Result"]}

    # Have to manually add absolutePath with a separate request
    r = get(host + "/data/experiments/%s/scans/%s/resources/DICOM/files" % (session, scanid), params={"format": "json", "locator": "absolutePath"})
    for dicom in r.json()["ResultSet"]["Result"]:
        dicomFileDict[dicom['Name']]['absolutePath'] = dicom['absolutePath']

    ##########
    # Download DICOMs
    print "Downloading files for scan %s." % scanid
    os.chdir(scanDicomDir)

    # Check secondary
    # Download any one DICOM from the series and check its headers
    # If the headers indicate it is a secondary capture, we will skip this series.
    dicomFileList = dicomFileDict.items()

    (name, pathDict) = dicomFileList[0]
    download(name, pathDict)

    print 'Checking modality in DICOM headers of file %s.' % name
    d = dicomLib.read_file(name)
    modalityHeader = d.get((0x0008, 0x0060), None)
    if modalityHeader:
        print 'Modality header: %s' % modalityHeader
        modality = modalityHeader.value.strip("'").strip('"')
        if modality == 'SC' or modality == 'SR':
            print 'Scan %s is a secondary capture. Skipping.' % scanid
            continue
    else:
        print 'Could not read modality from DICOM headers. Skipping.'
        continue

    ##########
    # Download remaining DICOMs
    for name, pathDict in dicomFileList[1:]:
        download(name, pathDict)

    os.chdir(builddir)
    print 'Done downloading for scan %s.' % scanid
    print

    ##########
    # Prepare NIFTI directory structure
    scanBidsDir = os.path.join(bidsdir, scanid)
    if not os.path.isdir(scanBidsDir):
        print 'Creating scan NIFTI BIDS directory %s.' % scanBidsDir
        os.mkdir(scanBidsDir)

    scanImgDir = os.path.join(imgdir, scanid)
    if not os.path.isdir(scanImgDir):
        print 'Creating scan NIFTI image directory %s.' % scanImgDir
        os.mkdir(scanImgDir)

    # Remove any existing files in the builddir.
    # This is unlikely to happen in any environment other than testing.
    for f in os.listdir(scanBidsDir):
        os.remove(os.path.join(scanBidsDir, f))

    for f in os.listdir(scanImgDir):
        os.remove(os.path.join(scanImgDir, f))

    # Convert the differences
    bidsname = base + bidsname
    print "Base " + base + " series " + seriesdesc + " match " + bidsname

    print 'Converting scan %s to NIFTI...' % scanid
    # Do some stuff to execute dcm2niix as a subprocess
    print subprocess.check_output("dcm2niix -b y -z y -f {} -o {} {}".format(bidsname, scanBidsDir, scanDicomDir).split())
    print 'Done.'

    # Move imaging to image directory
    for f in os.listdir(scanBidsDir):
        if "nii" in f:
            os.rename(os.path.join(scanBidsDir, f), os.path.join(scanImgDir, f))

    # Check number of files in image directory, if more than one assume multiple echoes
    numechoes = len(os.listdir(scanImgDir))  # multiple .nii.gz files will be generated by dcm2niix if there are multiple echoes
    if numechoes > 1:
        # Loop through set of folders (IMG and BIDS)
        for dir in (scanImgDir, scanBidsDir):
            # Get sorted list of files
            multiple_echoes = sorted(os.listdir(dir))

            # Divide length of file list by number of echoes to find out how many files in each echo
            # (Multiband DWI would have BVEC, BVAL, and JSON in BIDS dir for each echo)
            filesinecho = len(multiple_echoes) / numechoes

            echonumber = 1
            filenumber = 1

            # Rename files
            for echo in multiple_echoes:
                splitname = echo.split("_")

                # Locate run if present in BIDS name
                runstring = [s for s in splitname if "run" in s]

                if runstring != []:
                    runindex = splitname.index(runstring[0])
                    splitname.insert(runindex, "echo-" + str(echonumber))  # insert where run is (will displace run to later position)
                else:
                    splitname.insert(-1, "echo-" + str(echonumber))  # insert right before the data type

                # Remove the "a" or other character from before the .nii.gz if not on the first echo
                if (echonumber > 1):
                    ending = splitname[-1].split(".")
                    cleanedtype = ending[0][:-1]
                    ending[0] = cleanedtype
                    cleanedname = ".".join(ending)
                    splitname[-1] = cleanedname

                # Rejoin name
                echoname = "_".join(splitname)

                # Do file rename
                os.rename(os.path.join(dir, echo), os.path.join(dir, echoname))

                # When file count rolls over increment echo and continue
                if filenumber == filesinecho:
                    echonumber += 1
                    filenumber = 1  # restart count for new echo

                # Increment file count each time one is renamed
                filenumber += 1

    ##########
    # Upload results
    print
    print 'Preparing to upload files for scan %s.' % scanid

    # If we have a NIFTI resource and we've reached this point, we know overwrite=True.
    # We should delete the existing NIFTI resource.
    if hasNifti:
        print "Scan %s has a preexisting NIFTI resource. Deleting it now." % scanid

        try:
            queryArgs = {}
            if workflowId is not None:
                queryArgs["event_id"] = workflowId
            r = sess.delete(host + "/data/experiments/%s/scans/%s/resources/NIFTI" % (session, scanid), params=queryArgs)
            r.raise_for_status()

            r = sess.delete(host + "/data/experiments/%s/scans/%s/resources/BIDS" % (session, scanid), params=queryArgs)
            r.raise_for_status()
        except (requests.ConnectionError, requests.exceptions.RequestException) as e:
            print "There was a problem deleting"
            print "    " + str(e)
            print "Skipping upload for scan %s." % scanid
            continue

    # Uploading
    print 'Uploading files for scan %s' % scanid
    queryArgs = {"format": "NIFTI", "content": "NIFTI_RAW", "tags": "BIDS"}
    if workflowId is not None:
        queryArgs["event_id"] = workflowId
    if uploadByRef:
        queryArgs["reference"] = os.path.abspath(scanImgDir)
        r = sess.put(host + "/data/experiments/%s/scans/%s/resources/NIFTI/files" % (session, scanid), params=queryArgs)
    else:
        queryArgs["extract"] = True
        (t, tempFilePath) = tempfile.mkstemp(suffix='.zip')
        zipdir(dirPath=os.path.abspath(scanImgDir), zipFilePath=tempFilePath, includeDirInZip=False)
        files = {'file': open(tempFilePath, 'rb')}
        r = sess.put(host + "/data/experiments/%s/scans/%s/resources/NIFTI/files" % (session, scanid), params=queryArgs, files=files)
        os.remove(tempFilePath)
    r.raise_for_status()
 

    queryArgs = {"format": "BIDS", "content": "BIDS", "tags": "BIDS"}
    if workflowId is not None:
        queryArgs["event_id"] = workflowId
    if uploadByRef:
        queryArgs["reference"] = os.path.abspath(scanBidsDir)
        r = sess.put(host + "/data/experiments/%s/scans/%s/resources/BIDS/files" % (session, scanid), params=queryArgs)
    else:
        queryArgs["extract"] = True
        (t, tempFilePath) = tempfile.mkstemp(suffix='.zip')
        zipdir(dirPath=os.path.abspath(scanBidsDir), zipFilePath=tempFilePath, includeDirInZip=False)
        files = {'file': open(tempFilePath, 'rb')}
        r = sess.put(host + "/data/experiments/%s/scans/%s/resources/BIDS/files" % (session, scanid), params=queryArgs, files=files)
        os.remove(tempFilePath)
    r.raise_for_status()

    ##########
    # Clean up input directory
    print
    print 'Cleaning up %s directory.' % scanDicomDir
    for f in os.listdir(scanDicomDir):
        os.remove(os.path.join(scanDicomDir, f))
    os.rmdir(scanDicomDir)

    print
    print 'All done with image conversion.'

##########
# Generate session-level metadata files
previouschanges = ""

# Remove existing files if they are there
print "Check for presence of session-level BIDS data"
r = get(host + "/data/experiments/%s/resources" % session, params={"format": "json"})
sessionResources = r.json()["ResultSet"]["Result"]
print 'Found resources %s.' % ', '.join(res["label"] for res in sessionResources)

# Do initial checks to determine if session-level BIDS metadata is present
hasSessionBIDS = any([res["label"] == "BIDS" for res in sessionResources])

if hasSessionBIDS:
    print "Session has preexisting NIFTI resource. Deleting previous BIDS metadata if present now."

    # Consider making CHANGES a real, living changelog
    # r = get( host + "/data/experiments/%s/resources/BIDS/files/CHANGES"%(session) )
    # previouschanges = r.text
    # print previouschanges

    try:
        queryArgs = {}
        if workflowId is not None:
            queryArgs["event_id"] = workflowId

        r = sess.delete(host + "/data/experiments/%s/resources/BIDS" % session, params=queryArgs)
        r.raise_for_status()
    except (requests.ConnectionError, requests.exceptions.RequestException) as e:
        print "There was a problem deleting"
        print "    " + str(e)
        print "Skipping upload for session-level files."

# Fetch metadata from project
rawprojectdata = get(host + "/data/projects/%s" % project, params={"format": "json"})
projectdata = rawprojectdata.json()

# Build dataset description
dataset_description = OrderedDict()
dataset_description['Name'] = project

dataset_description['BIDSVersion'] = BIDSVERSION

# License- to be added later on after discussion of sensible default options
# dataset_description['License'] = None

# Compile investigators and PI into names list
invnames = []
invfield = [x for x in projectdata["items"][0]["children"] if x["field"] == "investigators/investigator"]
print str(invfield)

if invfield != []:
    invs = invfield[0]["items"]

    for i in invs:
        invnames.append(" ".join([i["data_fields"]["firstname"], i["data_fields"]["lastname"]]))

pifield = [x for x in projectdata["items"][0]["children"] if x["field"] == "PI"]

if pifield != []:
    pi = pifield[0]["items"][0]["data_fields"]
    piname = " ".join([pi["firstname"], pi["lastname"]])

    if piname in invnames:
        invnames.remove(piname)

    invnames.insert(0, piname + " (PI)")

if invnames != []:
    dataset_description['Authors'] = invnames

# Other metadata - to be added later on
# dataset_description['Acknowledgments'] = None
# dataset_description['HowToAcknowledge'] = None
# dataset_description['Funding'] = None
# dataset_description['ReferencesAndLinks'] = None

# Session identifier
dataset_description['DatasetDOI'] = host + '/data/experiments/' + session

# Upload
queryArgs = {"format": "BIDS", "content": "BIDS", "tags": "BIDS", "inbody": "true"}
if workflowId is not None:
    queryArgs["event_id"] = workflowId

r = sess.put(host + "/data/experiments/%s/resources/BIDS/files/dataset_description.json" % session, json=dataset_description, params=queryArgs)
r.raise_for_status()

# Generate CHANGES
changes = "1.0 " + time.strftime("%Y-%m-%d") + "\n\n - Initial release."

# Upload
h = {"content-type": "text/plain"}
r = sess.put(host + "/data/experiments/%s/resources/BIDS/files/CHANGES" % session, data=changes, params=queryArgs, headers=h)
r.raise_for_status()

# All done
print 'All done with session-level metadata.'
